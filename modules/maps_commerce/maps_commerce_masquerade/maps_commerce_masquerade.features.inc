<?php
/**
 * @file
 * maps_commerce_masquerade.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function maps_commerce_masquerade_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
