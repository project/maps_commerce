<?php
/**
 * @file
 * maps_commerce_demo.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function maps_commerce_demo_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'administer maps suite'.
  $permissions['administer maps suite'] = array(
    'name' => 'administer maps suite',
    'roles' => array(),
    'module' => 'maps_suite',
  );

  return $permissions;
}
