<?php
/**
 * @file
 * maps_commerce_demo.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function maps_commerce_demo_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['commerce_cart-cart'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'cart',
    'module' => 'commerce_cart',
    'node_types' => array(),
    'pages' => 'checkout*',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => 'header',
        'status' => 1,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'maps_admin' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'maps_admin',
        'weight' => 0,
      ),
      'maps_theme_commerce' => array(
        'region' => 'header',
        'status' => 1,
        'theme' => 'maps_theme_commerce',
        'weight' => -12,
      ),
    ),
    'title' => '<none>',
    'visibility' => 0,
  );

  $export['commerce_checkout_progress-indication'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'indication',
    'module' => 'commerce_checkout_progress',
    'node_types' => array(),
    'pages' => 'checkout/*
cart',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'bartik',
        'weight' => -10,
      ),
      'maps_admin' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'maps_admin',
        'weight' => -10,
      ),
      'maps_theme_commerce' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'maps_theme_commerce',
        'weight' => -13,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  $export['search-form'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'form',
    'module' => 'search',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => 'header',
        'status' => 1,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'maps_admin' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'maps_admin',
        'weight' => 0,
      ),
      'maps_theme_commerce' => array(
        'region' => 'header',
        'status' => 1,
        'theme' => 'maps_theme_commerce',
        'weight' => -11,
      ),
    ),
    'title' => '<none>',
    'visibility' => 0,
  );

  $export['system-user-menu'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'user-menu',
    'module' => 'system',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 3,
      ),
      'maps_admin' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'maps_admin',
        'weight' => 0,
      ),
      'maps_theme_commerce' => array(
        'region' => 'header',
        'status' => 1,
        'theme' => 'maps_theme_commerce',
        'weight' => -13,
      ),
    ),
    'title' => '<none>',
    'visibility' => 0,
  );

  $export['user-login'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'login',
    'module' => 'user',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => 'header',
        'status' => 1,
        'theme' => 'bartik',
        'weight' => 2,
      ),
      'maps_admin' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'maps_admin',
        'weight' => 0,
      ),
      'maps_theme_commerce' => array(
        'region' => 'header',
        'status' => 1,
        'theme' => 'maps_theme_commerce',
        'weight' => -13,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  return $export;
}
