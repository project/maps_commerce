<?php
/**
 * @file
 * maps_commerce_content.features.inc
 */

/**
 * Implements hook_commerce_product_default_types().
 */
function maps_commerce_content_commerce_product_default_types() {
  $items = array(
    'product' => array(
      'type' => 'product',
      'name' => 'Product',
      'description' => 'A basic product type.',
      'help' => '',
      'revision' => 1,
    ),
  );
  return $items;
}

/**
 * Implements hook_ctools_plugin_api().
 */
function maps_commerce_content_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function maps_commerce_content_node_info() {
  $items = array(
    'flat_product' => array(
      'name' => t('Flat product'),
      'base' => 'node_content',
      'description' => t('A <em>Flat Product</em> content displays a Commerce product that has no variation.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'marketing_product' => array(
      'name' => t('Marketing product'),
      'base' => 'node_content',
      'description' => t('A <em>Marketing Product</em> content displays several Commerce products as variations.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'page' => array(
      'name' => t('Basic page'),
      'base' => 'node_content',
      'description' => t('Use <em>basic pages</em> for your static content, such as an \'About us\' page.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
